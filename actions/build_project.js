const prompts = require( 'prompts' );

const { FileSystem } = require('../modules/FileSystem');
const { Project } = require('../modules/Project');
const { TemplateSystem } = require('../modules/TemplateSystem');

require( './../includes/questions' );

buildProject = async () => {

    let project = new Project();
    let fsys = new FileSystem();
    let ts = new TemplateSystem();

    const projects = await project.loadProjectsWithValue();
    
    const { qSelectProject } = getQuestions( projects );
    let projectSelected = await prompts( qSelectProject );

    // selected project
    projectSelected = projectSelected.project;

    //read project data.json
    const dataProjectFile = './project/' + projectSelected + '/data.json';
    const out = await fsys.getFile( dataProjectFile );
    const dataProject = JSON.parse( out );

    const buildDirectory = './build/' + projectSelected;
    const baseDirectory = __dirname + '/../template/back/base';
    const dynamicDirectory = __dirname + '/../template/back/dynamic';

    dataProject.entities.forEach( async entityNameFile => {

        //extract entity data
        const entityDataJson = await fsys.getFile( './entity/' + entityNameFile + '.json' );
        const entityData = JSON.parse( entityDataJson );

        // TODO: default: Express - Other frameworks ( Symfony, Django ) with promps option select
        //copy base back
        
        const buildDirectoryDb = './build/' + projectSelected + '/db';
        const buildDirectoryDomain = './build/' + projectSelected + '/domain';
        const buildDirectoryRoutes = './build/' + projectSelected + '/routes';
        
        const nameFileEntity = entityData.name + '.js';
        const nameFileEntityFilePath = buildDirectory + '/db/' + nameFileEntity;
        const nameFileDomainFilePath = buildDirectory + '/domain/' + nameFileEntity;
        const nameFileRoutesFilePath = buildDirectory + '/routes/' + nameFileEntity;

        fsys.makeDir( './build' );
        fsys.makeDir( buildDirectory );
        fsys.makeDir( buildDirectoryDb );
        fsys.makeDir( buildDirectoryDomain );
        fsys.makeDir( buildDirectoryRoutes );
        fsys.copyDir( baseDirectory, buildDirectory );

        //create db entity nodejs
        const entityDbOut = await ts.renderTemplate( dynamicDirectory + '/entity.js.twig', entityData );
        fsys.writeFile( nameFileEntityFilePath, entityDbOut );

        //create domain nodejs
        const domainOut = await ts.renderTemplate( dynamicDirectory + '/domain.js.twig', entityData );
        fsys.writeFile( nameFileDomainFilePath, domainOut );

        //create routes for entity nodejs
        const routesOut = await ts.renderTemplate( dynamicDirectory + '/routes.js.twig', entityData );
        fsys.writeFile( nameFileRoutesFilePath, routesOut );

        //console.log( entityData, nameFileEntityFilePath );
    });

    //create all routes
    const routesRoutesOut = await ts.renderTemplate( dynamicDirectory + '/routes_routes.js.twig', { entities: dataProject.entities } );
    fsys.writeFile( buildDirectory + '/routes/routes.js', routesRoutesOut );
}