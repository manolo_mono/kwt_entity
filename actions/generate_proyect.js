const prompts = require( 'prompts' );
const fs = require( 'fs' );

require( './../includes/questions' );

const { Entity } = require('./../modules/Entity');

generateProject = async () => {

    let entity = new Entity();

    const entities = await entity.loadEntities();

    let choices = [];

    for( var i = 0; i < entities.length; i++ ){
        
        choices.push({
            title: entities[i],
            value: entities[i].replace( '.json', '' )
        });
    }

    const { qProject } = getQuestions();
    const project = await prompts( qProject );

    const { qSelectEntities } = getQuestions( choices );

    const entitiesSelected = await prompts( qSelectEntities );

    //create project
    const dataProjectJson = JSON.stringify( entitiesSelected );
    const pathProject = './project/' + project.name;
    const nameFileProject = pathProject + '/data.json';

    if( ! fs.existsSync( './project' ) ) fs.mkdirSync( './project' );
    if( ! fs.existsSync( pathProject ) ) fs.mkdirSync( pathProject );

    fs.writeFile( nameFileProject, dataProjectJson, ( err ) => {

        if( err ) onErr( err );

        console.log( 'New project created !! [' + project.name + ']' , nameFileProject );
    });
}