const prompts = require( 'prompts' );
const fs = require( 'fs' );

require( './../includes/questions' );

const { 
    EntityInterface,
    Field
} = require('./../modules/EntityInterface');

let dataEntity = new EntityInterface();

const { qEntity, qField, qConfirm } = getQuestions();

createEntity = async () => {

    const entity = await prompts( qEntity );

    if( ! entity.name ) process.exit();

    dataEntity.setName( entity.name );

    const field = await addField();

    //show resume
    const dataEntityJson = JSON.stringify( dataEntity );
    const pathNameFileEntity = './entity/' + entity.name + '.json';

    if( ! fs.existsSync( './entity' ) ) fs.mkdirSync( './entity' );

    fs.writeFile( pathNameFileEntity, dataEntityJson, ( err ) => {

        if( err ) onErr( err );

        console.log( 'New model created!! ', pathNameFileEntity );
    });
}

const addField = async () => {

    const field = await prompts( qField );

    if( ! field.name ) process.exit();
    
    let newField = new Field();
    newField.setName( field.name );
    newField.setType( field.type );

    dataEntity.addField( newField );

    const confirm = await prompts( qConfirm );
    
    if( confirm.more_fields ) await addField();
    else{

        let activeField = new Field();
        activeField.setName( 'active' );
        activeField.setType( 'Boolean' );
        dataEntity.addField( activeField );
    }
}