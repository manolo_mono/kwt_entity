
const fs = require('fs');

listEntities = () => {

    fs.readdir( './entity', 'utf8', ( err, files_entities ) => {

        if( err ) return console.log( err );

        for( var i = 0; i < files_entities.length; i++ ){

            const entity_file = files_entities[i];
            const entity_file_path = './entity/' + entity_file;
            
            fs.readFile( entity_file_path, 'utf8', ( err, entity_json ) => {

                const json_entity_obj = JSON.parse( entity_json );

                if( err ) return console.log( err );

                console.log(json_entity_obj);
            });
        }
    });
}