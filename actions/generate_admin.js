const prompts = require( 'prompts' );
require( './../includes/questions' );

const User = require( './../template/back/base/model/User' );
const db_tools = require( './../template/back/base/tools/db_tool' );

const { qGenerateAdmin } = getQuestions();
const sha256 = require( 'sha256' );

generateAdmin = async () => {

    const admin = await prompts( qGenerateAdmin );

    const { name, email, password } = admin;

    const connect = await db_tools.DBConnectMongoose();
    const userExist = await User.findOne( { email } );

    if( ! userExist ){

        const hash = sha256.x2( password );
        admin.password  = hash;
        const user = new User( admin );
        const newUser = await user.save();

        if( newUser ) console.log( 'Usuario admin creado correctamente', name, email );
        else console.log( 'Error al insertar usuario' );

    }else console.log( 'El usuario ya existe' );

    connect.close();
}