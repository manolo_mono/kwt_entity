var express = require('express');
const jwt = require('jsonwebtoken');
var settings = require('../config');

const rutasProtegidas = express.Router();

rutasProtegidas.use( (req, res, next) => {
    
    const token = ( req.header('authorization') != undefined ? req.header('authorization').split(' ')[1] : false );

    if( token ){

        jwt.verify( token, settings.jwt.secret,

            ( err, decoded ) => {

                if( err ){
                    return res.json({ 
                            ok: false,
                            msg: 'Token inválido' 
                        });
                }else{
                    
                    req.decoded = decoded;
                    next();
                }
            }
        );

    }else{
        
      res.send({ 
          mensaje: 'Token inválido (2)' 
      });
    }    
 });

 module.exports = rutasProtegidas;