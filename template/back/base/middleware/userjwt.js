const { response } = require('express');
const User = require('../model/User');

const createUser = async ( req, res = response ) => {

    const { email, password } = req.body;
    let existUser = await User.findOne( { email } );

    if( ! existUser ){

        const hash = sha256.x2( password );
        req.body.password = hash;
        const user = new User( req.body );
        const newUser = await user.save();

        res.json({
            ok:true,
            hash:hash,
            user: newUser
        });

    }else{
        res.json({
            error: 'Ya existe un usuario'
        });
    }
}

module.exports = createUser;