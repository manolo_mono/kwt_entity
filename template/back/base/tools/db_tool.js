'use strict'

var mongoose = require('mongoose');
var config = require('../config');

var db;

exports.DBConnectMongoose = function(){

    return new Promise(function(resolve, reject) {
        
        mongoose.Promise = global.Promise;

        if (db) {
            return resolve(db);
        }

        // database connect
        mongoose.connect( config.dbMongo.url, {
            useNewUrlParser: true, 
            useCreateIndex: true,
            useUnifiedTopology: true
        } )
            .then(() => {
                db = mongoose.connection
                console.log('mongo connection created');
                resolve(db);
            })
            .catch(err => {
                console.log('error creating db connection: ' + err);
                reject(err);
            });
    });
};