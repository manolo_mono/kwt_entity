
'use strict';

var express = require( 'express' );
var bodyparser = require( 'body-parser' );
var cors = require( 'cors' );

var jwt = require('./routes/jwt');
var db_tools = require('./tools/db_tool');

var app = express();

db_tools.DBConnectMongoose()
    .then( () => {

        var routes = require( './routes/routes' );
        var port = 4000;

        // configure app to use bodyParser()
        // this will let us get the data from a POST
        app.use( bodyparser.urlencoded({extended: true}) );
        app.use( bodyparser.json({limit: '10mb'}) );
        app.use(cors());

        app.use( '/auth', jwt );
        routes.assignRoutes( app );

        app.listen( port );

        console.log('Server listening on port ' + port );
    })
    .catch(err => {
        console.log('Error AQUI: ', err)
    })