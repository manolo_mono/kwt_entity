const { response } = require('express');
const User = require('../../model/User');
const argon2 = require('argon2');
const { create } = require('../../model/User');

const createUser = async( req, res = response ) => {

    const { email, password } = req.body;

    let existUser = await User.findOne( { email } );

    if( ! existUser ){

        const hash = argon2.hash( password, {

            type: argon2.argon2d,
            memoryCost: 2 ** 16,
            hashLength: 50,

        }).then((hash) => {

            req.body.password = hash;

            try {
            
                const user = new User( req.body );
            
                user.save().then(
                    () => {
                        res.json({
                            ok:true,
                            hash:hash,
                            user: user
                        })
                }).catch( (error) => {
                    res.json(error);
                });
                
            } catch (error) {
                res.json(error);
            }
            
        }).catch( ( error ) => {
            res.json(error);
        }); 

    }else{
        res.json({
            error: 'Ya existe un usuario'
        });
    }
}

module.exports = createUser;