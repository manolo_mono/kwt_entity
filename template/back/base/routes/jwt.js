var express = require('express');
var router = express.Router();

const sha256 = require( 'sha256' );

const jwt = require('jsonwebtoken');
//const connection = require('../helper/dbMySql');
var auth = require('../middleware/auth');
var user = require('../model/User');

var settings = require('../config');

router.get( '/check', auth, ( req, res, next ) => {

    const token = ( req.header('authorization') ? req.header('authorization').split(' ')[1] : false );

    if( token ){

        jwt.verify( token, settings.jwt.secret,

            ( err, decoded ) => {

                if( err ){
                    
                    return res.json({
                        ok: false,
                        msg: err
                    });
                }else{

                    return res.json({
                        ok: true,
                        username: decoded.data.name,
                        _id: decoded.data._id
                    });
                }
            }
        );
    }
});

router.post( '/token', ( req, res, next ) => {

    const { email, password } = req.body;

    if( 
        email &&
        password
    ){

        const existUser = user.findOne({email}).then(
            ( user ) => {

                if( user ){

                    const hash = sha256.x2( password );

                    if( hash == user.password ){

                        //generate jwt token
                        const token = jwt.sign(
                            {
                                data: {
                                    name: user.name,
                                    _id: user._id
                                }
                            },
                            settings.jwt.secret,
                            {
                                expiresIn: settings.jwt.expires
                            }
                        );
                        
                        res.json({ ok:true, token: token, username: user.name, _id: user._id });

                    }else{

                        res.json({ ok:false });
                    }

                }else{
                    res.json({ok:false} );
                }
            }
        );
            
    }else{

        res.status.json({ok:false} );
    }
});

module.exports = router;