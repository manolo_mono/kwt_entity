
getQuestions = ( choices = false ) => {

    if( ! choices ) return {

            qGenerateAdmin: [
                {
                    type: 'text',
                    name: 'name',
                    message: 'Nombre de usuario',
                    validate: value => value ? true : 'Se necesita un nombre válido'
                },
                {
                    type: 'text',
                    name: 'email',
                    message: 'Email',
                    validate: value => value ? true : 'Se necesita un email'
                },
                {
                    type: 'password',
                    name: 'password',
                    message: 'Contraseña',
                    validate: value  => ( value.length >= 6 ) ? true : 'mínimo 6'
                },
            ],

            qProject: [
                {
                    type: 'text',
                    name: 'name',
                    message: 'Nombre del proyecto',
                    validate: value => value ? true : 'Se necesita un nombre válido'
                }
            ],

            qMenu:[
                {
                    type: 'select',
                    name: 'option',
                    message: 'Seleccione una opción',
                    choices: [
                        { title: 'Crear entidad', value: 'create_entity' },
                        { title: 'Crear proyecto', value: 'generate_project' },
                        { title: 'Construir proyecto', value: 'build_project' },
                        { title: 'Crear usuario administrador (JWT)', value: 'generate_admin' },
                        { title: 'Listar entidades', value: 'list_entities' }
                    ],
                    hint: '- Pulse "Enter" para seleccionar',
                    initial: 0
                }
            ],

            qEntity:{
                type: 'text',
                name: 'name',
                message: 'Nombre de la entidad',
                validate: value => value ? true : 'Se necesita un nombre válido'
            },
            qField: [
                {
                    type: 'text',
                    name: 'name',
                    message: 'Nombre del campo',
                    validate: value => value ? true : 'Se necesita un nombre válido'
                },
                {
                    type: 'select',
                    name: 'type',
                    message: 'Tipo de dato para el campo.',
                    choices: [
                        { title: 'String', value: 'String' },
                        { title: 'Number', value: 'Number' },
                        { title: 'Array', value: 'Array' },
                        { title: 'Boolean', value: 'Boolean' },
                        { title: 'mongoose.Schema.Types.ObjectId', value: 'mongoose.Schema.Types.ObjectId' }
                    ],
                    hint: '- Pulse "Enter" para seleccionar',
                    initial: 0
                }
            ],
            qConfirm:  {
                type: 'confirm',
                name: 'more_fields',
                message: '¿Quieres añadir más campos a la entidad?'
            }
        };
    else
        return {

                qSelectEntities : {
                    type: 'multiselect',
                    name: 'entities',
                    message: 'Seleccione las entidades',
                    choices:  choices,
                    instructions: false
                },

                qSelectProject : {
                    type: 'select',
                    name: 'project',
                    message: 'Seleccione el proyecto que quieres construir.',
                    choices:  choices,
                    hint: '- Pulse "Enter" para seleccionar',
                    initial: 0
                }
        }
}