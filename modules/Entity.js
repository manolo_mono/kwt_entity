
const fs = require( 'fs' );
const util = require( 'util' );

const readdir = util.promisify( fs.readdir );
readDir = async path => await readdir( path );

class Entity{
    
    async loadEntities(){
        return await readDir( './entity' );
    }

    addEntity( name ){
        this.entities.push( name );
    }

    getEntities(){
        return this.entities;
    }
}

module.exports = { Entity };