const Twig = require( 'twig' );
const util = require( 'util' );

const rendertemplate = util.promisify( Twig.renderFile );

renderTemplate = async ( path, data ) => await rendertemplate( path, data );

class TemplateSystem{
    
    async renderTemplate( template, data ){
        return await renderTemplate( template, data );
    }
}

module.exports = { TemplateSystem };