
class EntityInterface{

    constructor(){

        this.name = '';
        this.fields = [];
    }

    setName( name ){
        this.name = name;
    }

    addField( field ){
        
        this.fields.push( {...field } );
    }

    getFields(){
        return this.fields;
    }
}

class Field{

    constructor(){

        this.name = '';
        this.type = '';
    }

    setName( name ){
        this.name = name;
    }

    setType( type ){
        this.type = type;
    }
}

module.exports = { EntityInterface, Field };