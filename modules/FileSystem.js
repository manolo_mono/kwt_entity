const fs = require( 'fs' );
const fse = require('fs-extra');
const util = require( 'util' );

const readdir = util.promisify( fs.readdir );
const readFile = util.promisify( fs.readFile );

readDir = async path => await readdir( path );
readNewFile = async path => await readFile( path, 'utf8' );

class FileSystem{

    async getAutoLoads( path ){

        let requires = [];

        const files = await readDir( path );

        for( var i = 0; i < files.length; i++ ){

            const file = files[i];
            const fileSplit = file.split('.');
            const nameFile = fileSplit[0];
            const req = path + '/' + nameFile;
            
            requires.push( req );
        };

        return requires;
    }

    async getFile( path ){

        return await readNewFile( './' + path );
    }

    async copyDir( src, dest ){

        return fse.copySync( src, dest, { overwrite: true } );
    }

    makeDir( dir ){
        if( ! fs.existsSync( './' + dir ) ) fs.mkdirSync( './' + dir );
    }

    writeFile( path, content ){

        fs.writeFile( path, content, () => {} );
    }
    
}

module.exports = { FileSystem };