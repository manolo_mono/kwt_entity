
const fs = require( 'fs' );
const util = require( 'util' );

const readdir = util.promisify( fs.readdir );
readDir = async path => await readdir( path );

class Project{

    constructor () {
        
        console.log( 'New Project' );
    }

    async loadProjects(){
        return await readDir( './project' );
    }

    async loadProjectsWithValue(){

        let projectsWithValue = [];

        const projects = await readDir( './project' );

        projects.forEach( proj => {
            projectsWithValue.push({
                title: proj,
                value: proj
            });
        });

        return projectsWithValue;
    }
}

module.exports = { Project };