FROM node:14.16.0-stretch-slim

RUN apt update
RUN apt --yes --force-yes install openssh-server xorg 

RUN useradd -ms /bin/bash kiwi
RUN echo 'kiwi:kiwitools' | chpasswd

USER kiwi 

WORKDIR /home/kiwi

COPY ./package*.json /home/kiwi/
COPY ./entrypoint.sh /home/kiwi/entrypoint.sh

USER root
RUN chmod +x /home/kiwi/entrypoint.sh
EXPOSE 22

ENTRYPOINT ["bash", "/home/kiwi/entrypoint.sh"]
