## Kiwi Tools Web
[Kiwi Tools Web](https://kiwitools.es "Kiwi Tools Web")

## ¿Para que sirve Kwt Entity?
Para crear un backend API REST rápido y sencillo detallando el modelo al sistema.

## ¿Qué tecnología utiliza?
El backend lo genera en Nodejs conectando con MongoDB (Moongose).

# kwt - Entity - Instalación

```bash
npm i @kiwitools/kwt_entity
```

```javascript
// index.js
const { kwtEntity } = require( '@kiwitools/kwt_entity' );

const kwte = new kwtEntity();

kwte.start();
```

# Ejemplo creación API REST ( Trabajador / Grupo )

Vamos a crear un API de ejemplo. Las entidades a desarrollar serán dos: **Trabajador** y **Grupo**.

Los campos de **Trabajador** son 3: nombre, apellidos y **Grupo**.

El único campo para **Grupo**: nombre.


```bash
nodejs index.js
```

## 01 - Creación de entidad "Trabajador".

Seleccionamos **"Crear entidad"** y pulsaremos **"Enter"**, nos pedirá un nombre para la entidad, la llamaremos: **"Trabajador"**.
A continuación nombre del campo. El primer campo será el **"nombre"** con tipo de dato **"String"** nos pedirá si queremos añadir más campos **"y"**, el siguiente **"apellidos"** tipo **"String"** y por último el campo **"Grupo"**, en este caso le dirémos de tipo **"mongoose.Schema.Types.ObjectId"** y cuando nos pregunte si queremos otro campo le dirémos **"n"**. Ya tenemos creada la entidad **Trabajador**.

### resumen
![Entidad Trabajador](https://kiwitools.es/images/npm/trabajador.png)

## 02 - Creación de la entidad "Grupo".

```bash
nodejs index.js
```

Seleccionamos **"Crear entidad"** y pulsaremos **"Enter"**, nos pedirá un nombre para la entidad, la llamaremos: **"Grupo"**.
A continuación nombre del campo. El primer y último campo será **"nombre"** con tipo de dato **"String"** y cuando nos pregunte si queremos otro campo le dirémos **"n"**. Ya tenemos creada la entidad **Grupo**.

### resumen
![Entidad Grupo](https://kiwitools.es/images/npm/grupo.png)

## 03 - Creación del Proyecto TrabajadorGrupo.

Nuevamente ejecutaremos la aplicación.

```bash
nodejs index.js
```

Seleccionamos **"Crear proyecto"** y pulsaremos **"Enter"**, nos pedirá un nombre para el proyecto, lo llamaremos: **"TrabajadorGrupo"**.
A continuación nos aparecerá un listado con las entidades que hemos creado, con la barra espaciadora seleccionamos o quitamos selección, seleccionaremos las dos entidades que hemos creado anteriormente **Trabajador** y **Grupo**. Ya tenemos creado el proyecto.

### resumen
![Selección entidades](https://kiwitools.es/images/npm/proyecto-01.png)


## 04 - Construcción del proyecto TrabajadorGrupo (build).

```bash
nodejs index.js
```

Seleccionamos **"Construir proyecto"** y pulsaremos **"Enter"**, nos pedirá que proyecto queremos construir, seleccionaremos: **"TrabajadorGrupo"**.
Ya tenemos el proyecto contruido en *./build/TrabajadorGrupo* :).

### resumen
![Construcción proyecto TrabajadorGrupo](https://kiwitools.es/images/npm/build.png)

## 05 - Ejecución del proyecto TrabajadorGrupo.

Nos situaremos en la raiz del proyecto en *./build/TrabajadorGrupo* e instalaremos las dependencias.

```bash
cd build/TrabajadorGrupo
npm install
```

**NOTA:** Hay que destacar que este backend está pensado para una conexión *MongoDb*, así que si no hay corriendo un MongoDb en la máquina que se ejecuta no podría conectar por lo tanto no funciona la aplicación. De todas formas pongo el archivo *docker-compose.yml* por si quereís probarlo rápidamente.

```yaml
version: "3.7"

services:
  db:
    container_name: mongodb
    image: mongo
    restart: always
    ports:
      - 27017:27017
    logging:
      driver: none
    stdin_open: false
    tty: false
    volumes:
      - dbdata:/data/db
    #environment:
    #  MONGO_INITDB_ROOT_USERNAME: root
    #  MONGO_INITDB_ROOT_PASSWORD: example
    networks:
      - default
 
volumes:
  dbdata:
```

En la raiz del proyecto hay un archivo llamado *config.js* donde se puede cambiar los datos de conexión. En un futuro se podrá construir para distintos motores de búsqueda.

```javascript
const settings = {
    
    dbMongo:{
        url: 'mongodb://localhost:27017/kiwitools'
    },
    jwt:{
        secret: 'ghdyfg42182s122244996786783457634567347893457635675674565nvnbdsbh**nvsdvhdvhghgfdshpfkjf hnvmd.l-_.ñ,lñĺllllpj ---fffff01e09423f4e50a9fe080bdb1=78*.^aff8b16a582c21bfb43314a8609061e9448e62bbf92139=/8-:87',
        expires: 3600
    }
}

module.exports = settings;
```

Si ya tenemos *MongoDb* corriendo:

```bash
npm start
```

![Corriendo el proyecto](https://kiwitools.es/images/npm/start.png)

## 06 - Creando usuario administrador Json Web Token [JWT].

Necesitaremos un usuario para poder atacar al API **TrabajadorGrupo** , por ahora sólo está implementado **Json Web Token**, aquí sólo nos pedirá nombre de usuario, email y password.

Seleccionamos **"Crear usuario administrador (JWT)"** y pulsaremos **"Enter"**.

![Crear usuario administrador](https://kiwitools.es/images/npm/usuario-admin.png)

## 07 - Atacando al backend TrabajadorGrupo.

Por fin vamos a utilizar llamadas hacia la aplicación TrabajadorGrupo.

Lo primero necesitamos obtener el token de autenticación. Necesitaremos enviarle el nombre de usuario y la contraseña al endpoint */auth/token* vía ***POST***.

### Request
![Get token Request POST](https://kiwitools.es/images/npm/request-get-token.png)

### Response
![Get token Response](https://kiwitools.es/images/npm/response-get-token.png)

## 08 - Insertando un Grupo.

Vamos a crear un grupo en el endpoint */grupo* vía ***POST***. Ahora para poder autenticarnos y crear un **Grupo** necesitamos enviar el token en la cabecera **"Bear Token"** de la llamada.

![Header Bear Token](https://kiwitools.es/images/npm/bear-token.png)

### Request
![Create Grupo POST](https://kiwitools.es/images/npm/request-create-group.png)

### Response
![Create Grupo](https://kiwitools.es/images/npm/response-create-group.png)

Ya tenemos un grupo creado llamado **"Desarrollo"**. En la respuesta de la llamada podemos apreciar que devuelve un dato interesante que es el ***"_id"***, es el identificador único de *MongoDb*.


## 09 - Creando el Trabajador en el grupo "Desarrollo".

Os podeís imaginar cómo crear el Trabajador en el grupo "Desarrollo". En el campo "Grupo" del trabajador enviaremos el ***"_id"*** que nos devolvío la llmada de creación del grupo.

### Request
![Create Trabajador POST](https://kiwitools.es/images/npm/request-create-worker.png)

### Response
![Create Trabajador](https://kiwitools.es/images/npm/response-create-worker.png)




