const prompts = require( 'prompts' );
const { FileSystem } = require('./modules/FileSystem');

let fileSystem = new FileSystem();

class kwtEntity{

    async start(){
        
        //autoload includes
        const fileIncludes = await fileSystem.getAutoLoads( __dirname + '/includes' );
        fileIncludes.forEach(req => require( req ));

        //autoload actions
        const fileActions = await fileSystem.getAutoLoads( __dirname + '/actions' );
        fileActions.forEach(req => require( req ));

        //questions menu
        const { qMenu } = getQuestions();
        const option = await prompts( qMenu );

        const splitOption = option.option.split('_');
        const nameFunction = splitOption[0] + splitOption[1].charAt(0).toUpperCase() + splitOption[1].slice(1);

        //exec option 
        eval( nameFunction + '();');
    }
}

module.exports = { kwtEntity }